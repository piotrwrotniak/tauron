import csv
import requests
import json
import config
import os.path
import argparse


def login(session):
    print("Logging in...")
    session.get('https://logowanie.tauron.pl')
    r = session.post('https://logowanie.tauron.pl/login',
                     data={'username': config.username, 'password': config.password,
                           '_target_path': 'https://elicznik.tauron-dystrybucja.pl'})
    return is_logged_in(r)


def is_logged_in(response):
    login_form_part = '<input class="float-right" type="text" id="username" name="username" />'
    return login_form_part not in response.content.decode('utf-8')


def get_headers():
    return {'Accept': 'application/json, text/javascript, */*; q=0.01', 'Accept-Encoding': 'gzip, deflate, br',
            'Content-Type': 'application/x-www-form-urlencoded'}


def get_dane(date):
    return 'dane%5BsmartNr%5D=30780004676&dane%5BchartDay%5D={}&dane%5BcheckOZE%5D=on&dane%5BparamType%5D=day&dane%5BparamArea%5D=szczyt&dane%5Bcache%5D=0&dane%5BchartType%5D=2'.format(
        date)


def get_day_data(s, day):
    print('Fetching data for {}'.format(day))
    res = s.post('https://elicznik.tauron-dystrybucja.pl/index/charts', data=get_dane(day), headers=get_headers())
    return res.content.decode('utf-8')


def write_data(filename, data):
    with open(filename, 'a', newline='\n') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL, dialect='excel')
        writer.writerow([data['title'], data['sum'], data['sumOZE']])
        writer.writerow(('Godzina', 'Zuzycie', 'Oddany do sieci'))
        for d in data['data']:
            writer.writerow(d)
        writer.writerow([])


def parse_response(response):
    a = json.loads(response)

    data = a['dane']['chart']
    indexed = {int(k['Hour']): k['EC'] for k in data}

    oze_data = a['dane']['OZE']
    oze = {int(oze_data[str(k)]['Hour']): oze_data[str(k)]['EC'] for k in range(1, 25)}

    indexed = [(k, indexed.get(k, 0), oze.get(k, 0)) for k in range(1, 25)]

    return {'title': data[0]['Date'], 'data': indexed, 'sum': a['sum'], 'sumOZE': a['OZEValue']}


def flow(month_plus_year):
    session = requests.Session()
    logged_in = login(session)
    if logged_in:
        print("Logged in successfully")
    else:
        raise Exception("Could not log in, check configuration")

    csv_filename = '{}.csv'.format(month_plus_year)
    if os.path.isfile(csv_filename):
        print("Removing currently present file at path {}".format(csv_filename))
        os.remove(csv_filename)

    for current_day in range(1, 31):
        if current_day < 10:
            current_day = "0{}".format(current_day)
        r = get_day_data(session, "{}.{}".format(current_day, month_plus_year))
        try:
            d = parse_response(r)
            write_data(csv_filename, d)
        except Exception as e:
            print("Could not fetch data for {}.{} ({})".format(current_day, month_plus_year, str(e)))


def setup():
    parser = argparse.ArgumentParser()
    parser.add_argument("from_month", help="Month to start the processing from [1-12]")
    parser.add_argument("to_month", help="Last month to process [1-12]")
    parser.add_argument("year", help="Year to process")
    args = parser.parse_args()
    from_month, to_month, year = None, None, None
    examples = "examples: `1 3 2017`, `4 10 2017`, only one year per request is supported!"
    try:
        from_month = int(args.from_month)
        to_month = int(args.to_month)
        year = int(args.year)
        if from_month < 1 or from_month > 12 or to_month < 1 or to_month > 12 or from_month > to_month:
            raise Exception("Parsed but wrong numbers!")
    except ValueError:
        print("Provide 3 numbers! {}".format(examples))
    except Exception as e:
        print("{} {}".format(str(e), examples))
    return from_month, to_month, year


f, t, y = setup()
if f and t and y:
    for i in range(f, t + 1):
        s = str(i)
        if i < 10:
            s = "0" + s
        flow('{}.{}'.format(s, y))


# example = '{"name":{"chart":"Zu\\u017cycie 05.11.2017"},"dane":{"chart":[{"EC":"4.341","Date":"2017-11-05","Hour":"1","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"4.78","Date":"2017-11-05","Hour":"2","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"5.196","Date":"2017-11-05","Hour":"3","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"5.45","Date":"2017-11-05","Hour":"4","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"4.835","Date":"2017-11-05","Hour":"5","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"5.168","Date":"2017-11-05","Hour":"6","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"4.966","Date":"2017-11-05","Hour":"7","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"3.752","Date":"2017-11-05","Hour":"8","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"2.493","Date":"2017-11-05","Hour":"9","Status":"0","Extra":"0","Zone":"1","Taryfa":"C12a"},{"EC":"2.17","Date":"2017-11-05","Hour":"10","Status":"0","Extra":"0","Zone":"1","Taryfa":"C12a"},{"EC":".775","Date":"2017-11-05","Hour":"11","Status":"0","Extra":"0","Zone":"1","Taryfa":"C12a"},{"EC":"1.76","Date":"2017-11-05","Hour":"12","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":".916","Date":"2017-11-05","Hour":"13","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"1.219","Date":"2017-11-05","Hour":"14","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"2.971","Date":"2017-11-05","Hour":"15","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"5.177","Date":"2017-11-05","Hour":"16","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"5.188","Date":"2017-11-05","Hour":"17","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"5.684","Date":"2017-11-05","Hour":"18","Status":"0","Extra":"0","Zone":"1","Taryfa":"C12a"},{"EC":"5.285","Date":"2017-11-05","Hour":"19","Status":"0","Extra":"0","Zone":"1","Taryfa":"C12a"},{"EC":"5.657","Date":"2017-11-05","Hour":"20","Status":"0","Extra":"0","Zone":"1","Taryfa":"C12a"},{"EC":"5.687","Date":"2017-11-05","Hour":"21","Status":"0","Extra":"0","Zone":"1","Taryfa":"C12a"},{"EC":"5.541","Date":"2017-11-05","Hour":"22","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"4.343","Date":"2017-11-05","Hour":"23","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"},{"EC":"3.687","Date":"2017-11-05","Hour":"24","Status":"0","Extra":"0","Zone":"2","Taryfa":"C12a"}],"zone":[{"start":"2017-11-05 9","stop":"2017-11-05 12"},{"start":"2017-11-05 18","stop":"2017-11-05 22"}],"zoneN":2,"other":[],"OZE":[],"myAverage":[],"weather":[],"cloudiness":[]},"sum":97.041,"sumOther":0,"sumMyAverage":0,"OZEValue":0,"sLastReadOZE":"Stan licznika:","sumTarget":0,"pie":{"glowny":{"color":["#002E5F","#3C76B1"],"dane":[{"title":"szczyt","value":27.751},{"title":"poza szczytem","value":69.29}],"type":1,"name":"05.11.2017"}},"TargetValue":0,"TargetRange":0,"legend":"szczyt","countTarif":2,"sCompareTable":"","ok":1}'
# a = parse_response(example)
# print(a)
